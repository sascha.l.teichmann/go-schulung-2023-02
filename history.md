# Geschichte von Go

- 2007 internes Projekt von [Rob Pike](https://de.wikipedia.org/wiki/Rob_Pike), [Robert Griesemer](https://de.wikipedia.org/wiki/Robert_Griesemer), [Ken Thompson](https://de.wikipedia.org/wiki/Ken_Thompson)
- 2009 Open Source 0.9
- 2012 Go 1.0
- Alle halbe Jahre eine stabile Version
- Aktuell 1.21.4
- [14 Jahre](https://go.dev/blog/14years)
