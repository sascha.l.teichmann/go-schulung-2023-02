package main

import "fmt"

func main() {

	// var array [10]int
	//var barray [20]int
	//array := [...]int{
	array := [10]int{
		3: 1, 5: 3, 7: 67, 9: 90,
	}

	array[0] = 42
	array[11] = 42
	// barray = array

	fmt.Println(array)

	for i, v := range array {
		fmt.Println(i, v)
		array[i] = 0
	}
	fmt.Println(len(array))
}
