package greet

import (
	"fmt"

	_ "github.com/lib/pq"
)

var Prefix string = "hurra"

/*
var Prefix string

func init() {
	Prefix = "hurra"
}
*/

func GreetMe() {
	fmt.Println(Prefix, "Hallo from GreetMe")
	greetYou()
}

func init() {
	fmt.Println("init greet 1")
}

func init() {
	fmt.Println("init greet 2")
}

func init() {
	fmt.Println("init greet 3")
}
