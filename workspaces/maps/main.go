package main

import "fmt"

func main() {

	//var m map[string]int
	//m = make(map[string]int)
	m := map[string]int{
		"Otto": 0,
	}

	m["Klaus"] = 42
	m["Petra"] = 16

	fmt.Println(m)
	fmt.Println("Otto:", m["Otto"])
	fmt.Println("Karl:", m["Karl"])

	if x, ok := m["Otto"]; ok {
		fmt.Println("Otto:", x)
	}

	if x, ok := m["Karl"]; ok {
		fmt.Println("Karl:", x)
	}

	delete(m, "Otto")

	fmt.Println("---")
	for k, v := range m {
		fmt.Printf("%s: %d\n", k, v)
	}
	fmt.Println(len(m))
}
