package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type calculator struct {
	current float64
}

func (c *calculator) add(x float64) {
	c.current += x
}

func (c *calculator) sub(x float64) {
	c.current -= x
}

func (c *calculator) mul(x float64) {
	c.current *= x
}

var operators = map[string]func(*calculator, float64){
	"+": (*calculator).add,
	"-": (*calculator).sub,
	"*": (*calculator).mul,
}

func main() {

	calc := calculator{}

	sc := bufio.NewScanner(os.Stdin)

	for sc.Scan() {
		line := sc.Text()
		prefix, postfix, ok := strings.Cut(line, " ")
		if !ok {
			continue
		}
		value, err := strconv.ParseFloat(
			postfix, 64)
		if err != nil {
			log.Printf("bad input: %v\n", err)
			continue
		}
		if op := operators[prefix]; op != nil {
			op(&calc, value)
		}
		fmt.Println(calc.current)
	}

	if err := sc.Err(); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
