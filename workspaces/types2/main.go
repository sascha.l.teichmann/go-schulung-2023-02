package main

import "fmt"

type myType int

func (mt myType) printMe() {
	fmt.Println(mt)
}

type theirType float32

func (tt theirType) hello() {
	fmt.Println("Hello")
}

type yourType struct {
	myType // embedding
	theirType
}

func (yt yourType) hello() {
	fmt.Println("yourType hello")
	yt.theirType.hello()
}

func main() {
	var yt yourType

	yt.myType = 3

	yt.printMe()
	yt.hello()

	//_ = yt
}
