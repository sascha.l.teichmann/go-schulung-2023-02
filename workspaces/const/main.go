package main

import (
	"fmt"
	"time"
)

const (
	hello = "Hello"
	zahl  = 42 // untypisiert Default-Typ int

	fliess float64 = 42 // typisiert

	unklar = 3.1415 * 1e4 // unklar Default float64
)

const (
	a time.Duration = 1 + iota*2
	b
	c
	d
)

func main() {

	const pi = 3.1415

	x := unklar
	fmt.Printf("%T\n", x)

	var i int = unklar
	fmt.Printf("%T\n", i)

	fmt.Println(a, b, c, d)
}
