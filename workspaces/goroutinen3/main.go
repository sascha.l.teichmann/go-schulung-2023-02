package main

import (
	"fmt"
	"runtime"
	"sync"
)

func worker(
	wg *sync.WaitGroup,
	jobs <-chan int,
	no int,
) {
	defer wg.Done()
	for x := range jobs {
		fmt.Println(no, x)
	}
}

func main() {

	jobs := make(chan int)

	var wg sync.WaitGroup

	for i, n := 0, runtime.NumCPU(); i < n; i++ {
		go worker(&wg, jobs, i)
		wg.Add(1)
	}

	for i := 0; i < 100_000; i++ {
		jobs <- i
	}
	close(jobs)

	wg.Wait()

}
