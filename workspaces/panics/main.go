package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
)

var matcher = regexp.MustCompile(`^(a+)$`)

func main() {

	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		line := scanner.Text()
		if matcher.MatchString(line) {
			fmt.Println(line)
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
