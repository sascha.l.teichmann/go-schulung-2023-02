package main

import (
	"encoding/json"
	"fmt"
)

type myStruct struct {
	x int
	_ byte
	F float32 "json:\"f\""
}

func evalute(d myStruct) {
	fmt.Printf("%v\n", d)
	d.F = 1
}

func main() {

	var d = myStruct{
		F: 3.1415,
	}

	d.x = 42

	evalute(d)
	fmt.Printf("%v\n", d)

	b, _ := json.Marshal(d)
	fmt.Println(string(b))
}
