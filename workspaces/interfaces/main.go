package main

import "fmt"

type Auto interface {
	Anfahren()
	Bremsen()
}

func fahren(a Auto) {
	a.Anfahren()
	a.Bremsen()
}

type car int

func (c car) Anfahren() {
	fmt.Println("VROOOM!")
}

func (c car) Bremsen() {
	fmt.Println("QUIETSCH!")
}

type brakes rune

func (b brakes) Bremsen() {
	fmt.Println("brakes: QUIETSCH!")
}

type motor string

func (m motor) Anfahren() {
	fmt.Println("motor: VROOOM!")
}

func main() {

	var a Auto

	var c car

	a = c

	fahren(a)

	b := struct {
		motor
		brakes
	}{}

	fahren(b)
}
