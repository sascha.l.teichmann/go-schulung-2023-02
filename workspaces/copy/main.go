package main

import "fmt"

func main() {

	slice := make([]int, 10)
	slice[0] = 1
	slice[3] = 5
	fmt.Println(slice)
	copy(slice[4:], slice[:2])
	fmt.Println(slice, slice[0])
}
