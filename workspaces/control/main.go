package main

import (
	"fmt"
)

// /* In C */ int calculate(int x, int y) {

func calculate(x, y int) int64 {

	//return int64(x * y) // Type conversion
	return int64(x) * int64(y) // Type conversion
}

func condition(a, b int) bool {
	return a < b
}

func control(a, b int) {
	fmt.Println()
	fmt.Println()
	fmt.Println()

	if c := condition(a, b); c {
		fmt.Println("a < b", c)
	} else if a == b {
		fmt.Println("a == b", c)
	} else {
		fmt.Println("a >= b", c)
	}

	for i := 0; i < 10; i++ {
		fmt.Println(i)
	}

	for {
		// code ...
		if a-b > 4 {
			break
		}
	}

outer:
	for i := 0; i < 100; i++ {
		for j := 0; j < 100; j++ {
			//inner:
			if i > 50 {
				continue outer
			}
		}
	}

	goto outer

	// Variante 1
	switch x := a * 2; x {
	case 1:
		fmt.Println("one")
		//fallthrough
	case 2:
		fmt.Println("one")
	case 3, 4:
		fmt.Println("3 or 4")
	default:
		fmt.Println("none of these")
	}

	// Variante 2 ~> if/else-Ketten
	switch y := a * 10; {
	case a < 20:
	case a < 10:
	case a < 4 || b > 10:
		fmt.Println(y)
	}

}

func main() {

	// int8, int16, int32, int64
	// uint8, uint16, uint32, uint64
	// byte (uint8)
	// float32, float64
	// complex64, complex128
	// string
	// bool
	// uintptr
	// rune (int32)

	var (
		w int
		x float32
	)

	// var z int = calculate(10, 23)

	z := calculate(10, 23)

	//z, w = w, z

	fmt.Println(z)

	_, _ = x, w

	control(10, 20)
	control(10, 10)

}
