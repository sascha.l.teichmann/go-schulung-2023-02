package main

import (
	"fmt"
)

func main() {

	//var ch chan bool
	// ch = make(chan bool)
	done := make(chan struct{})

	go func() {
		// defer close(done)

		fmt.Println("Hello from go routine 1")

		done <- struct{}{}
	}()

	_, ok := <-done

	fmt.Println(ok)

	//time.Sleep(time.Second)
}
