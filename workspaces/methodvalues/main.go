package main

import (
	"fmt"
	"net/http"
)

type controller struct {
	greet string
}

func (c *controller) hello(
	rw http.ResponseWriter,
	req *http.Request,
) {
	fmt.Println(req.Method)
	fmt.Fprintln(rw, c.greet)
}

func main() {

	mux := http.NewServeMux()

	ctrl := controller{
		greet: "Hello from controller",
	}

	mux.HandleFunc("/hello", ctrl.hello)

	/*
		mux.HandleFunc("/hello",
			func(rw http.ResponseWriter, req *http.Request) {
				ctrl.hello(rw, req)
			})
	*/

	http.ListenAndServe("localhost:8080", mux)
}
