package main

import "fmt"

func calculate(a, b int) (x, y int) {
	x, y = a*b, -42
	return
}

func hyperCalculate(a, b int) (x, y int) {
	x, y = a*a*b*b, -42
	return
}

func add(x int) func() int {
	// closure binding
	return func() int {
		y := x
		x += 3
		return y
	}
}

func call(fn func(int, int) (int, int)) {
	x, y := fn(10, 20)
	fmt.Println(x, y)
}

func iterate(
	slice []int,
	op func(int, int) int,
) int {
	x := 0
	for _, y := range slice {
		x = op(x, y)
	}
	return x
}

func main() {
	//x := calculate(10, 20)
	y, x := calculate(10, 20)
	fmt.Println(y, x)

	//var fn func(int, int) (int, int)

	fn := calculate
	fn(1, 2)
	call(hyperCalculate)
	call(fn)

	a := add(79)
	fmt.Println("a:", a())
	fmt.Println("a:", a())
	fmt.Println("a:", a())

	fmt.Println(iterate(
		[]int{1, 2, 3, 4},
		func(a, b int) int { return a + b }))
}
