package main

import "fmt"

func modify(slice []int) {
	slice[0]++
}

func appendMe(slice *[]int) {
	*slice = append(*slice,
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
	fmt.Println("append: len:", len(*slice))
	fmt.Println("append: cap:", cap(*slice))
}

func main() {
	var slice []int
	fmt.Println(slice)

	var backing [10]int

	slice = backing[1 : len(backing)/2]
	backing[0] = 42
	fmt.Println(slice)
	fmt.Println(slice[0])
	modify(slice)
	fmt.Println(slice[0])
	fmt.Println("len:", len(slice))
	fmt.Println("cap:", cap(slice))
	slice = slice[:len(slice)+1]
	fmt.Println("len:", len(slice))
	fmt.Println("cap:", cap(slice))
	//slice = slice[:len(slice)+10]

	n := 10

	slice = make([]int, 3, n)
	fmt.Println("len:", len(slice))
	fmt.Println("cap:", cap(slice))

	slice = append(slice, 1, 3, 5, 7, 89, 89, 98, 88)
	fmt.Println(slice)
	fmt.Println(len(slice))
	fmt.Println(cap(slice))

	appendMe(&slice)
	fmt.Println(len(slice))
	fmt.Println(cap(slice))

}
