package main

import "fmt"

type myInt int

func (mi myInt) printMe() {
	fmt.Printf("printMe: %d\n", mi)
}

func (mi *myInt) modify() {
	//fmt.Println("Gleich knalls")
	*mi = 42
}

func main() {

	var x myInt

	z := int(67)

	x = 3

	x = myInt(z)

	var y float32 = 3

	fmt.Println(x, y)

	x.printMe()
	//(&x).modify()
	x.modify()
	x.printMe()

	//var bad *myInt
	//bad.modify()
}
