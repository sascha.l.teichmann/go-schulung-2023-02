package main

import "fmt"

func main() {

	sum := make(chan int)

	jobs := make(chan int)

	go func() {
		var s int
		defer func() { sum <- s }()
		for x := range jobs {
			fmt.Println(x)
			s += x
		}
	}()

	for i := 0; i < 10; i++ {
		jobs <- i
	}
	close(jobs)

	result := <-sum
	fmt.Println(result)
}
