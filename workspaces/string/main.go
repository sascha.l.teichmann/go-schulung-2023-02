package main

import "fmt"

func main() {
	var s string

	s = "A 💩Hello"

	fmt.Printf("%s %c %c\n", s, s[0], s[2])
	fmt.Println(len(s))
	fmt.Println()
	for i, c := range s {
		fmt.Printf("%d '%c'\n", i, c)
	}

	fmt.Println()
	runes := []rune(s)
	for i, c := range runes {
		fmt.Printf("%d '%c'\n", i, c)
	}

	// s[0] = 'B' Wrong
	runes[0] = 'B'
	s = string(runes)
	fmt.Println()
	fmt.Println(s)
}
