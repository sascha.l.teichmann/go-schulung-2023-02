package main

import "fmt"

func modify(x *int) {
	*x = 42
}

func badC() *int {
	var local int
	return &local
}

func main() {

	var x int

	modify(&x)

	fmt.Println(x)

	//modify(nil)

	*badC() = 3

	y := new(int)

	_ = y

}
