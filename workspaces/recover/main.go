package main

import (
	"fmt"
	"log"
)

func level1() {
	level2()
}

func level2() {
	level3()
}

func level3() {
	panic("BAD BAD")
}

func API() (result int, err error) {

	defer func() {
		if p := recover(); p != nil {
			err = fmt.Errorf("API failed: %v", p)
		}
		//fmt.Println("Hello from defer", recover())
	}()

	level1()

	return 42, nil
}

func main() {
	x, err := API()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	fmt.Println(x)
}
