package main

import "fmt"

func main() {

	ch1 := make(chan int)
	ch2 := make(chan int)

	go func() {
		for i := 1; i <= 10; i++ {
			ch1 <- i
		}
		close(ch1)
	}()

	go func() {
		for i := 1; i <= 20; i++ {
			ch2 <- i
		}
		close(ch2)
	}()

	open := 1<<1 | 1

done:
	for {
		switch {
		case open == 1<<1|1:
			select {
			case x, ok := <-ch1:
				if !ok {
					open &= ^1
				} else {
					fmt.Println("ch1:", x)
				}
			case x, ok := <-ch2:
				if !ok {
					open &= ^2
				} else {
					fmt.Println("ch2:", x)
				}
				//default:
			}
		case open == 1:
			if x, ok := <-ch1; !ok {
				break done
			} else {
				fmt.Println("ch1:", x)
			}
		case open == 1<<1:
			if x, ok := <-ch2; !ok {
				break done
			} else {
				fmt.Println("ch1:", x)
			}
		}

	}
}
