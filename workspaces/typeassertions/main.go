package main

import "fmt"

type myInt int

func (mi myInt) String() string {
	return fmt.Sprintf("myInt %d", mi)
}

func printMe(arg any) {
	fmt.Println(arg)

	// x := arg.(int)
	if x, ok := arg.(int); ok {
		fmt.Printf("%T\n", x)
	}

	switch x := arg.(type) {
	case int:
		fmt.Printf("int %T %x\n", x, x*3)
	case float64:
	case string:
	case fmt.Stringer:
		fmt.Printf("fmt.Stringer: %s\n", x.String())
	default:
		fmt.Printf("default %T\n", x)
	}
}

func main() {
	printMe(1)
	printMe(1.234289)
	printMe("Hello")
	printMe(myInt(32))
}
