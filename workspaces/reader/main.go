package main

import (
	"io"
	"log"
	"os"
)

type filter struct {
	io.Reader
}

func (f *filter) Read(b []byte) (int, error) {
	n, err := f.Reader.Read(b)
	for i, x := range b[:n] {
		if x == 'A' {
			b[i] = 'a'
		}
	}
	return n, err
}

func main() {
	if _, err := io.Copy(os.Stdout, &filter{os.Stdin}); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
