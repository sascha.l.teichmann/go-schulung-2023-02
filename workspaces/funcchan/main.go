package main

import "fmt"

type server struct {
	//databaseMu sync.Mutex
	database map[string]string
	calls    chan func(*server)
	quit     bool
}

func newServer() *server {
	return &server{
		database: map[string]string{},
		calls:    make(chan func(*server)),
	}
}

func (s *server) run(done chan struct{}) {

	defer close(done)

	for call := range s.calls {
		call(s)
		if s.quit {
			break
		}
	}
}

func (s *server) value(key string) string {
	//s.databaseMu.Lock()
	//defer s.databaseMu.Unlock()

	result := make(chan string)

	s.calls <- func(s *server) {
		result <- s.database[key]
	}

	return <-result
}

func (s *server) store(key, value string) {
	//s.databaseMu.Lock()
	//defer s.databaseMu.Unlock()

	s.calls <- func(s *server) {
		s.database[key] = value
	}
}

func (s *server) kill() {
	s.calls <- func(s *server) { s.quit = true }
}

func main() {

	s := newServer()

	done := make(chan struct{})

	go s.run(done)

	go func() {
		s.store("Hello", "World")
		fmt.Println(s.value("Hello"))
		s.kill()
	}()

	<-done
}
