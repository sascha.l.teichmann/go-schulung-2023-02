package main

import (
	"errors"
	"fmt"
	"log"
)

// err-lang

func calculate(a, b int) (int, error) {
	if b == 0 {
		return 0, errors.New("divide by zero")
	}
	return a / b, nil
}

func main() {
	for _, div := range []int{2, 0} {
		if x, err := calculate(10, div); err != nil {
			log.Fatalf("error: %v\n", err)
		} else {
			fmt.Printf("result: %d\n", x)
		}
	}
}
