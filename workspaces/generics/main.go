package main

import (
	"cmp"
	"fmt"
)

type node[T any] struct {
	left  *node[T]
	right *node[T]
	val   T
}

type tree[T any] struct {
	root *node[T]
	less func(a, b T) bool
}

func (t *tree[T]) visit(visit func(T)) {
	var recurse func(n *node[T])
	recurse = func(n *node[T]) {
		if n == nil {
			return
		}
		recurse(n.left)
		visit(n.val)
		recurse(n.right)
	}
	recurse(t.root)
}

func min[T cmp.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}

type myInt int

func main() {
	y := myInt(12)
	z := myInt(23)
	v := min(y, z)
	fmt.Println(v)
	x := min[float64](10, float64(23.23))
	//x := min("abc", "def")
	fmt.Printf("%T\n", x)

	intTree := tree[int]{
		less: func(a, b int) bool { return a < b },
	}

	_ = intTree
}
