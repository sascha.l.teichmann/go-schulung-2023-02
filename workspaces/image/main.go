package main

import (
	"fmt"
	"image"
	"log"
	"os"

	_ "image/png"
)

func main() {

	for _, fname := range os.Args[1:] {
		f, err := os.Open(fname)
		if err != nil {
			log.Fatalf("error: %v\n", err)
		}
		img, _, err := image.Decode(f)
		f.Close()
		if err != nil {
			log.Fatalf("error: %v\n", err)
		}

		if si, ok := img.(interface {
			SubImage(image.Rectangle) image.Image
		}); ok {
			ni := si.SubImage(image.Rect(0, 0, 10, 10))
			fmt.Printf("%v\n", ni.Bounds())
		}
	}
}
