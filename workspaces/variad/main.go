package main

import "fmt"

func sum(greet string, args ...int) int {
	fmt.Println(greet)
	s := 0
	for _, x := range args {
		s += x
	}
	return s
}

func main() {
	fmt.Println(sum("Hello, world!", 1, 2, 4, 19, 89))
	fmt.Println(sum("Hello, moon!"))

	slice := []int{1, 5, 7}
	fmt.Println(sum("Hello, slice!", slice...))

}
